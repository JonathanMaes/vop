from adfil import move, read_data, beamforming, filter, run_L_n, filter_same_weights, add, beamform_white_noise, frequencies, reconstruct_FFT, MVDR_file
import matplotlib.pyplot as plt
import numpy as np


def test_move(fileName):
    x = read_data(fileName)[0][0]
    t = 1.5
    y = move(x, t)
    plt.plot(np.arange(0, len(x), 1), x)
    plt.plot(np.arange(0, len(x), 1), y)
    plt.show()

fileName = 'data/arrayaandrone+klap/Audiotrack-0-16000.wav'
# test_move(fileName)
# filter(fileName)
# filter(os.path.dirname(fileName), n=400, L=30)
# run_L_n(fileName)
# filter_same_weights(fileName)
# add(os.path.dirname(fileName))
# add(os.path.dirname(fileName), weights=[6, 1, 1, 1, 1, 1, 1])
# filter('output/add/sum_arrayaandrone+klap_[6, 1, 1, 1, 1, 1, 1].wav')
# filter('output/beamforming/beamforming_Matige_snelheid_met_spraak_th45.00_ph0.00.wav')

# fileName = 'data/Matige_snelheid_met_spraak.wav'
# basename = os.path.splitext(os.path.basename(fileName))[0]
# # filter(fileName)
# # beamforming(fileName, 45, 0)
# beamforming(fileName, 0, 0)
# # beamforming('output/n200L60/Processed_%s.wav' % basename, 45, 0)
# beamforming('output/n200L60/Processed_%s.wav' % basename, 0, 0)
# # run_L_n('output/beamforming/beamforming_Processed_%s_th45.00_ph0.00.wav' % basename)
# run_L_n('output/beamforming/beamforming_%s_th45.00_ph0.00.wav' % basename)

fileName = 'data/posities/recht eronder.wav'
# beamforming(fileName, 0, 0)
# beamforming(fileName, 90, 0)

# beamforming('data/posities/45rechts.wav', 45, 90, saveSpectrum=[-30, -10], fileFormat=".png")
# beamforming('data/posities/45rechts.wav', 45, -90, saveSpectrum=[-30, -10], fileFormat=".png")
# beamforming('data/posities/45links.wav', 45, 90, saveSpectrum=[-30, -10], fileFormat=".png")
# beamforming('data/posities/45links.wav', 45, -90, saveSpectrum=[-30, -10], fileFormat=".png")

# print(weights_MVDR(covariance_matrix(read_data(fileName)[0]), 0, 0, 4000))
# frequencies('data/Arrayaandrone+klap.wav')

# for theta in [0, 45, 90]:
#     beamform_white_noise(theta, 0, 'gaussian')

# n, L = [20, 40, 100, 200, 400, 700, 1000, 2000], [0, 2, 10, 40, 80, 200, 500, 2000]
# run_L_n(fileName, n=n, L=L, oneChannel=True)


# # fileName = ['data/metingen/spraakmetmatigonder.wav', 'data/Matige_snelheid_met_spraak.wav']
# # noiseFileName = ['data/metingen/spraakmetmatigonder.wav', 'data/noise/Matige_snelheid_met_spraak-noise.wav']
# fileName = ['data/Matige_snelheid_met_spraak.wav']
# noiseFileName = ['data/noise/Matige_snelheid_met_spraak-noise.wav']
# # thetas = [0, 45]
# thetas = [0]
# # frequencies(fileName, plot=True, limits=[-10, 10], save=True)
# # beamforming(fileName, 45, 0, saveSpectrum=[-10, 10])
# # beamforming('output/n200L60/Processed_Matige_snelheid_met_spraak.wav', 45, 0, saveSpectrum=[-10,10])
# # frequencies('output/n200L60/Processed_beamforming_Matige_snelheid_met_spraak_th45_ph0.wav', plot=True, limits=[-10, 10], save=True)

# for i in range(len(fileName)):
#     for t in thetas:
#         MVDR_file(fileName[i], noiseFile=noiseFileName[i], theta=t, phi=0, K=256, alpha=0.9)
#         MVDR_file(fileName[i], noiseFile=noiseFileName[i], theta=t, phi=0, K=256)
#         # MVDR_file(fileName[i], noiseFile=noiseFileName[i], theta=t, phi=0, K=64)



# ### MVDR BEAMFORMING ###
# alpha = 0.99
# MVDR_file("data/8-05/matig_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=alpha)
# MVDR_file("data/8-05/matig_met_spraak_hoek_45.wav", theta=45, phi=0, alpha=alpha)
# MVDR_file("data/8-05/matig_met_spraak_hoek_45-30.wav", theta=45, phi=30, alpha=alpha)
# MVDR_file("data/8-05/matig_varierend_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=alpha)
# MVDR_file("data/8-05/vollekracht_met_spraak.wav", theta=0, phi=0, alpha=alpha)
# MVDR_file("data/8-05/vollekracht_met_spraak_2.wav", theta=0, phi=0, alpha=alpha)
# MVDR_file("data/8-05/vollekracht_met_spraak_hoek_45.wav", theta=45, phi=0, alpha=alpha)
# MVDR_file("data/8-05/vollekracht_met_spraak_hoek_45-30.wav", theta=45, phi=30, alpha=alpha)

# MVDR_file("data/8-05/matig_met_spraak_hoek_45.wav", theta=45, phi=0, alpha=0.95)
# MVDR_file("data/8-05/matig_met_spraak_hoek_45.wav", theta=45, phi=0, alpha=0.99)

# MVDR_file("data/8-05/matig_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=0.99)

# ### MVDR BEAMFORMING PLOTS ###
# for folder in ["output/MVDR", "data/8-05"]:
#     for file in os.listdir(folder):
#         if os.path.splitext(file)[1] == ".wav":
#             frequencies(os.path.join(folder, file), plot=False, save=True, L=2048, K=128)
# frequencies(r"output/MVDR\MVDR_matig_met_spraak_hoek_0_th0_ph0_alpha99%.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0])
# frequencies("data/8-05/matig_met_spraak_hoek_0.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
# frequencies("data/8-05/matig_met_spraak_hoek_45.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
# frequencies("data/8-05/matig_met_spraak_hoek_45-30.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
# frequencies("data/8-05/matig_varierend_met_spraak_hoek_0.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
# frequencies("data/8-05/vollekracht_met_spraak.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
# frequencies("data/8-05/vollekracht_met_spraak_2.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
# frequencies("data/8-05/vollekracht_met_spraak_hoek_45.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
# frequencies("data/8-05/vollekracht_met_spraak_hoek_45-30.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])

# ### DS BEAMFORMING PLOTS ###
# frequencies("data/Matige_snelheid_met_spraak.wav", plot=False, save=True, L=2048, K=128, limits=[-10, 10])
# frequencies("output/beamforming/beamforming_Matige_snelheid_met_spraak_th45_ph0.wav", plot=False, save=True, L=2048, K=128, limits=[-10, 10])
# frequencies("output/beamforming/beamforming_Processed_Matige_snelheid_met_spraak_th45_ph0.wav", plot=False, save=True, L=2048, K=128, limits=[-10, 10])
# frequencies("output/n200L60/Processed_beamforming_Matige_snelheid_met_spraak_th45_ph0.wav", plot=False, save=True, L=2048, K=128, limits=[-10, 10])
# frequencies("output/beamforming/beamforming_45rechts_th45_ph90.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0])
# frequencies("output/beamforming/beamforming_45rechts_th45_ph-90.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0])
# frequencies("output/beamforming/beamforming_45links_th45_ph90.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0])
# frequencies("output/beamforming/beamforming_45links_th45_ph-90.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0])
