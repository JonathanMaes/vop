import math
import matplotlib
import os
import matplotlib.pyplot as plt
import numpy as np
import soundfile as sf
from matplotlib import cm

def read_data(file):
    '''
    Leest een .wav bestand met één of meerdere kanalen in.
    @param file: locatie van een individueel .wav bestand
    @return: lijst van lijsten, met elke lijst een kanaal van de opname
    '''
    if os.path.isfile(file):
        data, fs = sf.read(file)
        data = data.transpose()
        data = data if isinstance(data[0], np.ndarray) else np.array([data])
        return [data, fs]

def NLMS(file, n, L, mu=0.1, plot=False, weightHistory=False, oneChannel=False):
    '''
    Filtert een bestand door middel van NLMS.
    @param file: relatief pad naar het te filteren bestand
    @param n: lengte van de lineaire combinatie in het NLMS-algoritme
    @param L: delay voor het NLMS-algoritme
    @param mu [0.1]: stapgrootte voor het NLMS-algoritme
    @param plot [True]: indien True worden x, y en y-x geplot, anders niet
    @param weightHistory [False]: indien True wordt de geschiedenis van w, dus
        de waarden van w voor elk sample, teruggegeven. Indien False wordt
        enkel de laatste waarde voor w teruggegeven.
    @param oneChannel [False]: indien True wordt enkel het eerste kanaal van
        <file> gefilterd, indien False worden alle kanalen gefilterd.
    @return: tuple(gefilterd signaal, fout, gewichten)
    '''
    print('n=%d, L=%d' % (n, L))
    x = read_data(file)[0]
    y, e = [], []
    for i, w in enumerate(x):
        xi = x[i]
        l = len(xi)
        yi = np.zeros(l)
        w = np.zeros(n)
        if weightHistory:
            w_history = np.zeros([l, n])
        ei = np.zeros(l)
        for k in range(n+L,l):
            yi[k] = np.dot(w, xi[k-n-L:k-L])
            if weightHistory:
                w_history[k] = w
            ei[k] = xi[k] - yi[k]
            w = np.add(w, 2*mu*ei[k]*xi[k-n-L:k-L]/(np.dot(xi[k-n-L:k-L],xi[k-n-L:k-L])+0.01))
        y.append(yi)
        e.append(ei)
        if plot:
            plt.subplot(1, 3, 1)
            plt.plot(xi)
            plt.title('x')
            plt.subplot(1, 3, 2)
            plt.plot(yi)
            plt.title('y')
            plt.subplot(1, 3, 3)
            plt.plot(yi-xi)
            plt.title('y-x')
            plt.show()
        if oneChannel:
            break
    return y, e, (w_history if weightHistory else w)

def NLMS_w(file, n, L, w, mu=0.1):
    '''
    Filtert een bestand, door middel van een gegeven reeks gewichten w.
    @param file: relatief pad naar te lezen bestand
    @param n: lengte van de lineaire combinatie in het NLMS-algoritme
    @param L: delay voor het NLMS-algoritme
    @param w: reeks te gebruiken gewichten voor elk sample
    @param mu [0.1]: stapgrootte voor het NLMS-algoritme
    @return: tuple(gefilderd signaal, fout)
    '''
    print('n=%d, L=%d' % (n, L))
    x = read_data(file)[0][0]
    l = len(x)
    y = np.zeros(l)
    e = np.zeros(l)
    for k in range(n+L,l):
        y[k] = np.dot(w[k], x[k-n-L:k-L])
        e[k] = x[k] - y[k]
    return y, e

def delay(van, naar, theta, phi, c=344, r=None):
    '''
    Berekent het verschil in aankomsttijd (in seconden) voor een signaal
    afkomstig uit een hoek (theta, phi) tussen microfoon <van> en <naar>
    (het signaal komt dus aan in <naar> <return> seconden na aankomst in <van>
    indien de array zich in het verre veld van de bron bevindt). De invals-
    richting is de vector van de microfoonarray naar de bron.
    @param van: nummer van eerste microfoon, met geldige index voor de lijst r
        (default is dit dus in [0, 1, 2, 3, 4, 5, 6])
    @param naar: nummer van tweede microfoon, met geldige index voor de lijst
        r (default is dit dus in [0, 1, 2, 3, 4, 5, 6])
    @param theta: hoek tussen invalsrichting en de neerwaartse verticale [deg]
    @param phi: hoek (in graden) in het horizontale vlak, met de x-as gericht
        van MIC0 naar midden(1, 6) en de y-as gericht van MIC0 naar MIC2 [deg]
    @param c [344]: geluidssnelheid [m]
    @param r [0.043*[[0, 0, 0], [math.sqrt(3)/2, .5, 0], [0, 1, 0], 
        [-math.sqrt(3)/2, .5, 0], [-math.sqrt(3)/2, -.5, 0], [0, -1, 0], 
        [math.sqrt(3)/2, -.5, 0]]]: locaties van microfoons op het
        microfoonrooster [cm]
    @return: verschil in aankomsttijd bij <naar> t.o.v. <van> [s]
    '''
    theta, phi = math.pi/180*theta, math.pi/180*phi
    if r is None:
        r = 0.043*np.array([[0, 0, 0], [math.sqrt(3)/2, .5, 0], [0, 1, 0], [-math.sqrt(3)/2, .5, 0], [-math.sqrt(3)/2, -.5, 0], [0, -1, 0], [math.sqrt(3)/2, -.5, 0]])
    a = np.array([math.sin(theta)*math.cos(phi), math.sin(theta)*math.sin(phi), math.cos(theta)])
    return np.dot(a, r[van] - r[naar])/c

def move(x, t, sinc_size=30):
    '''
    Berekent de ideale interpolatie van x, bij translatie over t samples (i.e.
    return[0] = x[t], return[1] = x[t+1] ... waarbij t een float mag zijn,
    waarbij x[t] dan de ideale interpolatie tussen x[math.floor(t)] en
    x[math.ceil(t)] voorstelt).
    @param x: origineel signaal
    @param t: aantal samples delay voor x
    @param sinc_size [30]: lengte aan weerskanten van de sinc array waarmee
        geconvolueerd (is dat een woord?) wordt
    @return: translatie van x over t samples
    '''
    t_int, t_frac = math.floor(t), t%1
    sinc = np.sinc([t_frac+n for n in range(-sinc_size, sinc_size+1)])  # sinc(30.5) = 0.0104
    x_op_tussenposities = np.convolve(x, sinc) # x_op_tussenposities[0] = x[t_frac], dus een interpolatie tussen x[0] en x[1], en zo voort
    if -sinc_size < t_int < sinc_size: # Als we over t_int kunnen verschuiven in x_op_tussenposities zelf (< en niet <= want slicing-formule werkt niet meer bij randen)
        return x_op_tussenposities[sinc_size+t_int:-sinc_size+t_int]
    elif t <= -sinc_size: # Als we niet kunnen terugschuiven zo ver, voeg dan een paar nullen toe voor de array en slice vanachter zoals normaal
        return np.append(np.zeros(sinc_size+t_int), x_op_tussenposities[:-sinc_size+t_int])
    elif t >= sinc_size: # Als we niet kunnen verderschuiven zo ver, voeg dan een paar nullen toe na de array en slice vanvoor zoals normaal
        return np.append(x_op_tussenposities[sinc_size+t_int:], np.zeros(t_int-sinc_size))
    # Hopelijk klopt dit een beetje, want ik kan er zelf al bijna niet meer aan uit.

def beamform(data, delta_t, theta, phi, weights=None):
    '''
    Berekent het resultaat van delay-and-sum-beamforming voor de reeks <data>,
    voor beamforming in de richting (<theta>, <phi>).
    @param data: een lijst van lijsten, waarbij elke lijst het signaal in 1
        van de microfoons van de array voorstelt
    @param delta_t: tijdsinterval tussen elk opeenvolgend sample
    @param theta: hoek tussen invalsrichting en de neerwaartse verticale [deg]
    @param phi: hoek (in graden) in het horizontale vlak, met de x-as gericht
        van MIC0 naar midden(1, 6) en de y-as gericht van MIC0 naar MIC2 [deg]
    @param weights [[1, ..., 1]]: lijst met gewichten voor elk
        kanaal respectievelijk
    @return: ge-delay-and-sum-beamformde som van alle kanalen (genormaliseerd)
    '''
    data = data[:7]
    if weights is None:
        weights = np.ones(7)
    delays = [delay(0, i, theta, phi) for i in range(0, 7)] # In seconden
    data_new = [move(channel, delays[i]/delta_t)*weights[i] for i, channel in enumerate(data)]  # Kanalen verplaatst in de tijd, zodat alles uit (theta, phi) op hetzelfde tijdstip staat (t.o.v. MIC0)
    return np.sum(np.array(data_new), axis=0)/np.sum(weights)

def dft(data, L=512, K=256):
    '''
    Geeft een 3D array terug met de fouriertransformatie van <data>. <data> is
    een 2D array, namelijk een lijst van samplewaarden voor elk kanaal.
    De 3D array die wordt teruggegeven bevat de fouriercoëfficiënten voor elke
    frequentie uit np.fft.rfftfreq(<L>), voor het signaal waarop een hamming-
    filter wordt toegepast. Het signaal wordt onderverdeeld in delen met
    lengte <L>, waarbij op elk deel een hammingfilter wordt toegepast.
    @param data: lijst van lijsten, waarbij deze laatste de samples zijn van
        een bepaalde microfoon
    @param L [512]: lengte van 1 segment waarvan de fouriertransformatie
        genomen wordt
    @param K [256]: afstand waarover verschoven wordt om het volgende segment
        te bekomen voor de berekening van de fouriertransformatie
    @return: 3D-array, met in de eerste dimensie de verschillende kanalen, de
        tweede dimensie de tijd (onderverdeling per 512 samples), en in de
        derde dimensie de dft voor elke frequentie voor dat deel en kanaal.
    '''
    dft = [np.fft.rfft(data[:,i:i+L]*np.hanning(L)) for i in range(0, len(data[0])-L+1, K)]
    dft = np.moveaxis(dft, [0, 1], [-2, -3]) # t-MIC-f => MIC-t-f (data in axis=0,1,2)
    return dft

def covariance_matrix(noise, L=512, K=256, alpha=1):
    '''
    Berekent de covariantiematrix voor elke frequentie in de dft (L=512 dus
    aantal covariantiematrices = 257 (voor alle frequenties van 0 tot 8000))
    @param noise: ruis om de covariantiematrix te berekenen, gegeven als een
        lijst van lijsten, met elke lijst een microfoonkanaal
    @param L [512]: lengte van 1 segment waarvan de fouriertransformatie
        genomen wordt
    @param K [256]: afstand waarover verschoven wordt om het volgende segment
        te bekomen voor de berekening van de fouriertransformatie
    @param alpha [1]: verhouding van aanpassing van de covariantiematrix R per
        segment-stap tegenover het behoud van de huidige covariantiematrix
    @return: lijst van matrices, elke matrix komt overeen met een frequentie
        uit de DFT. Elke matrix is de covariantiematrix voor die frequentie,
        uitgemiddeld over de tijd.
    '''
    if len(noise) > 7:
        noise = noise[:7]
    fourierTransform = dft(noise, L=L, K=K)
    frequencies = [fourierTransform[:,:,i].T for i in range(len(fourierTransform[0][0]))] # 1e dim: freq, 2e dim: tijd, 3e dim: kanalen
    if alpha != 1:
        R = [np.array([np.mean([np.transpose([N])*[N.conj()] for N in freq], axis=0) for freq in frequencies])] # seed-matrix: beginnen met zelfde matrix als anders volledig
        for i in range(len(frequencies[0])):
            R.append(R[-1]*alpha + np.array([np.transpose([freq[i]])*[freq[i].conj()] for freq in frequencies])*(1-alpha)) # lijst met matrices, elke matrix is R voor een frequentie
        R = R[1:]
    else:
        R = [np.mean([np.transpose([N])*[N.conj()] for N in freq], axis=0) for freq in frequencies] # lijst met matrices, elke matrix is R voor een frequentie
    return np.array(R)

def weights_MVDR(R, theta, phi, fs=16000, L=512, alpha=1):
    '''
    Berekent de gewichten voor het MVDR algoritme, gebruik makende van de
    covariantiematrix van de te onderdrukken ruis <R>, voor beamforming in een
    welbepaalde (<theta>, <phi>)-richting bij frequentie <f>.
    @param R: covariantiematrix van de ruis, berekend door covariance_matrix()
        (R wordt niet hier berekend om dubbele berekeningen te vermijden)
        (werkt zowel voor lijst van covariantiematrices corresponderend met
        verschillende frequenties, als een enkele covariantiematrix)
    @param theta: hoek tussen invalsrichting en de neerwaartse verticale [deg]
    @param phi: hoek (in graden) in het horizontale vlak, met de x-as gericht
        van MIC0 naar midden(1, 6) en de y-as gericht van MIC0 naar MIC2 [deg]
    @param fs [16000]: samplefrequentie
    @param L [512]: lengte van 1 segment waarvan de fouriertransformatie
        genomen wordt
    @param alpha [1]: verhouding van aanpassing van de covariantiematrix R per
        segment-stap tegenover het behoud van de huidige covariantiematrix
    @return: gewichten voor MVDR procedure, indien alpha nul is is dit een
        lijst met als elementen de gewichten voor de verschillende microfoons
        bij een bepaalde frequentie, indien alpha != 1 is dit een lijst
        waarvan de elementen de gewichten zijn als alpha wel nul zou zijn
        (alpha == 1: freq -> kanalen, alpha != 1: tijd -> freq -> kanalen)
    '''
    if len(R.shape) == 2:
        R = np.array([R])
    if alpha != 1:
        w_MVDR = []
        for tijdstip_R in R:
            w = []
            freqs = np.fft.rfftfreq(L)*fs
            for f, R_freq in enumerate(tijdstip_R):
                R_inv = np.linalg.pinv(R_freq)
                e = np.array([[np.exp(-1j*2*math.pi*freqs[f]*delay(i, 0, theta, phi)) for i in range(0, 7)]]).T # Zelfde als in theoretical_beamforming.py
                w.append(np.matmul(R_inv, e)/(np.matmul(np.matmul(e.conj().T, R_inv), e)))
            w_MVDR.append(np.array(w))
        return np.array(w_MVDR)
    else:
        w_MVDR = []
        freqs = np.fft.rfftfreq(L)*fs
        for f, R_freq in enumerate(R):
            R_inv = np.linalg.pinv(R_freq)
            e = np.array([[np.exp(-1j*2*math.pi*freqs[f]*delay(i, 0, theta, phi)) for i in range(0, 7)]]).T # Zelfde als in theoretical_beamforming.py
            w_MVDR.append(np.matmul(R_inv, e)/(np.matmul(np.matmul(e.conj().T, R_inv), e)))
        w_MVDR = np.array(w_MVDR)
        # print(np.dot(w_MVDR[-1].conj().T, e)) # Controle of dit 1 uitkomt
        return w_MVDR[0] if w_MVDR.shape[0] == 1 else w_MVDR

def MVDR(data, noise=None, theta=0, phi=0, fs=16000, L=512, K=256, alpha=1):
    '''
    Berekent het resultaat van MVDR op <data>, waarbij de covariantiematrix R
    berekend wordt voor <noise>. MVDR wordt toegepast voor beamformhoeken
    <theta> en <phi>.
    @param data: lijst van lijsten, waarbij elke lijst een microfoonkanaal
        voorstelt
    @param noise [data]: ruis om de covariantiematrix te berekenen, gegeven
        als een lijst van lijsten, met elke lijst een microfoonkanaal
    @param theta [0]: hoek invalsrichting en neerwaartse verticale [deg]
    @param phi [0]: hoek (in graden) in het horizontale vlak, voor een x-as
        van MIC0 naar midden(1, 6) en de y-as gericht van MIC0 naar MIC2 [deg]
    @param fs [16000]: samplefrequentie van data, nodig voor berekening w
    @param L [512]: lengte van 1 segment waarvan de fouriertransformatie
        genomen wordt
    @param K [256]: afstand waarover verschoven wordt om het volgende segment
        te bekomen voor de berekening van de fouriertransformatie
    @param alpha [1]: verhouding van aanpassing van de covariantiematrix R per
        segment-stap tegenover het behoud van de huidige covariantiematrix
    @return: lijst met als elementen de amplitude van het geluidssignaal na
        toepassing van het MVDR algoritme
    '''
    if len(data) > 7:
        data = data[:7]
    if noise is None or alpha != 1:
        noise = data
    elif len(noise) > 7:
        noise = noise[:7]
    data_dft = dft(data, L=L, K=K)
    R = covariance_matrix(noise, L=L, K=K, alpha=alpha)
    w = weights_MVDR(R, theta, phi, fs=fs, L=L, alpha=alpha)
    if alpha != 1:
        nieuwe_dft = np.array([[tijdstip*(w[i,:,k].conj().T)[0] for i, tijdstip in enumerate(kanaal)] for k, kanaal in enumerate(data_dft)])
    else:
        nieuwe_dft = np.array([kanaal*(w[:,k].conj().T)[0] for k, kanaal in enumerate(data_dft)]) # Apply w to all frequencies
    totaal = np.sum(nieuwe_dft, axis=0)
    nieuwe_data = [np.fft.irfft(tijdstip) for tijdstip in totaal] # Inverse fourier op basis van nieuwe_dft
    x = [np.arange(i, i+L) for i in range(0, len(data[0])-L+1, K)] # Indices overeenkomstig met elke lijst in nieuwe_data
    y = np.zeros((len(data[0])//L)*L) # Nullen even lang als data
    for i, indices in enumerate(x):
        y[indices] += nieuwe_data[i]
    return y*(K/L)*2

##############################################################################
def filter(path, n=200, L=60):
    '''
    Filtert het bestand <path>, of alle .wav bestanden in de map <path>, met
    een bepaalde n en L.
    @writes: "output/Processed_<file>_n<n>L<L>.wav" or "output/<dir>/Processed_<file>_n<n>L<L>.wav"
    @param path: relatief pad naar te verwerken bestand
    @param n [200]: lengte van de lineaire combinatie in het NLMS-algoritme
    @param L [60]: delay voor het NLMS-algoritme
    '''
    outdir = 'output/n%dL%d' % (n, L)
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    if os.path.isfile(path):
        baseName = os.path.splitext(os.path.basename(path))[0]
        fs = read_data(path)[1]
        _, e, _ = NLMS(path, n, L, plot=False)
        sf.write(os.path.join(outdir, 'Processed_%s.wav' % baseName), np.transpose(e), fs)
    elif os.path.isdir(path):
        dirName = os.path.basename(path)
        outdir = os.path.join(outdir, dirName)
        if not os.path.isdir(outdir):
            os.makedirs(outdir)
        for file in os.listdir(path):
            if file.endswith(".wav"):
                baseName = os.path.splitext(os.path.basename(file))[0]
                fs = read_data(file)[1]
                _, e, _ = NLMS(os.path.join(path, file), n, L, plot=False)
                sf.write(os.path.join(outdir, 'Processed_%s.wav' % baseName), np.transpose(e), fs)

def run_L_n(file, n=None, L=None, oneChannel=False):
    '''
    Filtert 1 bestand met verschillende n en L.
    @writes: "output/L-test/Processed_<file>_n<n>L<L>.wav"
    @param file: relatief pad naar te verwerken bestand
    @param n [[50, 100, 200, 300, 400]]: alle te testen waarden voor de lengte
        van de lineaire combinatie in het NLMS-algoritme
    @param L [[30, 60, 120]]: alle te testen waarden voor de delay voor het
        NLMS-algoritme
    @param oneChannel [False]: indien True wordt enkel het eerste kanaal van
        <file> gefilterd, indien False worden alle kanalen gefilterd.
    '''
    n_values = [50, 100, 200, 300, 400] if n is None else n
    L_values = [30, 60, 120] if L is None else L

    baseName = os.path.splitext(os.path.basename(file))[0]
    fs = read_data(file)[1]
    outdir = os.path.join('output/run_L_n', baseName)
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    for L in L_values:
        for n in n_values:
            _, e, _ = NLMS(file, n, L, plot=False, oneChannel=oneChannel)
            sf.write(os.path.join(outdir, 'Processed_%s_n%dL%d.wav' % (baseName, n, L)), np.transpose(e), fs)

def filter_same_weights(baseFile, n=200, L=60):
    '''
    Filtert alle bestanden in dezelfde map als baseFile, gebruik makend van
    dezelfde w als voor baseFile.
    @writes: "output/w_n<n>L<L>/Processed_<file>.wav"
    @param baseFile: relatief pad naar het bestand waarvan de gewichten worden
        gebruikt voor alle andere .wav bestanden in dezelfde map
    @param n [200]: lengte van de lineaire combinatie in het NLMS-algoritme
    @param L [60]: delay voor het NLMS-algoritme
    '''
    folder = os.path.dirname(baseFile)
    outdir = 'output/w_n%dL%d' % (n, L)
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    _, _, w = NLMS(baseFile, n, L, plot=False, weightHistory=True, oneChannel=True)

    for file in os.listdir(folder):
        if file.endswith(".wav"):
            baseName = os.path.splitext(os.path.basename(file))[0]
            fs = read_data(baseFile)[1]
            _, e = NLMS_w(os.path.join(folder, file), n, L, w)
            sf.write(os.path.join(outdir, 'Processed_%s.wav' % baseName), np.transpose(e), fs)

def add(path, weights=None):
    '''
    Berekent het gewogen gemiddelde van alle bestanden in 1 map.
    (Voor het gemiddelde van alle kanalen in 1 bestand in plaats van een map,
    gebruik beamforming(path, 0, 0, weights))
    @writes: "output/add/sum_<path>_<weights>.wav"
    @param path: relatief pad naar map waarvan alle .wav bestanden worden uitgemiddeld
    @param weights [[1, ..., 1]]: lijst met gewichten voor elk
        kanaal respectievelijk
    '''
    outdir = 'output/add'
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    
    if os.path.isdir(path):
        l = [file for file in os.listdir(path) if file.endswith(".wav")]
        if weights is None:
            weights = np.ones(len(l))
        data = [read_data(os.path.join(path, file))[0][0]*weights[i] for i, file in enumerate(l[:len(weights)])]
        fs = read_data(os.path.join(path, l[0]))[1]
    elif os.path.isfile(path):
        data, fs = read_data(path)
        if weights is None:
            weights = np.ones(len(data))
        data = [channel*weights[i] for i, channel in enumerate(data[:len(weights)])]

    e = np.sum(np.array(data), axis=0)/np.sum(weights)
    sf.write(os.path.join(outdir, 'sum_%s_%s.wav' % (os.path.splitext(os.path.basename(path))[0], weights)), e, fs)

def beamforming(path, theta, phi, weights=None, saveSpectrum=False, fileFormat=".pdf"):
    '''
    Berekent de gebeamformde versie van het bestand in <path>, waarbij een
    sinusoïdaal signaal afkomstig uit (theta, phi) onveranderd blijft.
    @writes: "output/beamforming/beamforming_<path>_th<theta>_ph<phi><weights if weights!=None>.wav"
    @writes: "output/beamforming/beamforming_<path>_th<theta>_ph<phi><weights if weights!=None>.pdf"
    @param path: relatief pad naar het bestand dat gebeamformd wordt
    @param theta: hoek tussen invalsrichting en de neerwaartse verticale [deg]
    @param phi: hoek (in graden) in het horizontale vlak, met de x-as gericht
        van MIC0 naar midden(1, 6) en de y-as gericht van MIC0 naar MIC2 [deg]
    @param weights [[1, ..., 1]]: lijst met gewichten voor elk
        kanaal respectievelijk
    @param saveSpectrum [False]: wordt doorgegeven als het argument 'limits'
        in de functie frequencies() indien het een lijst is, een plot van het
        spectrum wordt opgeslagen indien bool van saveSpectrum gelijk is aan
        True, indien dit False is niet
    @param fileFormat [".pdf"]: bestandsformaat van opgeslagen spectrum,
        indien saveSpectrum == True
    '''
    outdir = 'output/beamforming'
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    
    if os.path.isfile(path):
        data, fs = read_data(path)
        e = beamform(data, 1/fs, theta, phi, weights=weights)
        outFile = os.path.join(outdir, 'beamforming_%s_th%d_ph%d%s.wav' % (os.path.splitext(os.path.basename(path))[0], theta, phi, '_w%s'%weights if (weights != None) else ''))
        sf.write(outFile, e, fs)
        if saveSpectrum:
            if isinstance(saveSpectrum, list):
                frequencies(outFile, plot=True, save=True, limits=saveSpectrum, fileFormat=fileFormat)
            else:
                frequencies(outFile, plot=False, save=True, fileFormat=fileFormat)

def beamform_white_noise(theta, phi, distribution='gaussian', size=16000*4):
    '''
    Genereert witte ruis en filtert deze vervolgens via beamforming. Hierbij
    is de witte ruis hetzelfde bij elke microfoon, dus lijkt het alsof deze
    afkomstig is uit een bron bij theta=0.
    @writes: "output/noise_beamforming/noise_<distribution>_th<theta>_ph<phi>.wav"
    @param theta: hoek tussen invalsrichting en neerwaarste verticale [deg]
    @param phi: hoek (in graden) in het horizontale vlak, met de x-as gericht
        van MIC0 naar midden(1, 6) en de y-as gericht van MIC0 naar MIC2 [deg]
    @param distribution ['gaussian']: welke verdeling te gebruiken voor de
        ruis, is ofwel 'gaussian' of 'uniform'
    @param size [64000]: hoeveel samples de output lang is (= seconden/16000)
    '''
    outdir = 'output/noise_beamforming'
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    if distribution == 'gaussian':
        white_noise = np.random.normal(0, 0.1, size=size)
    elif distribution == 'uniform':
        white_noise = np.random.uniform(-0.5, 0.5, size=size)
    beamformed = beamform([white_noise for _ in range(8)], 1/16000, theta, phi)
    sf.write(os.path.join(outdir, 'noise_%s_th%d_ph%d.wav' % (distribution, theta, phi)), np.transpose([white_noise, beamformed]), 16000)

def frequencies(file, L=2048, plot=True, channel=0, save=False, limits=None, K=128, fileFormat='.pdf'):
    '''
    Plot het spectrum van het .wav bestand <file>: frequentie i.f.v. de tijd.
    @writes: "<file>.pdf" if save==True
    @param file: het .wav bestand waarvan het spectrum berekend wordt
    @param L [512]: lengte van 1 segment waarover de fouriertransformatie
        genomen wordt
    @param plot [True]: True indien het resultaat in realtime geplot moet
        worden, False indien niet
    @param channel [0]: welk kanaal geplot moet worden
    @param save [False]: True indien een plot van het spectrum moet opgeslagen
        worden, False indien niet
    @param limits [[-30, 10]]: dB-limiet voor kleurschakering in spectrum-plot
    @param K [128]: afstand waarover verschoven wordt om het volgende segment
        te bekomen voor de berekening van de fouriertransformatie
    @param fileFormat [".pdf"]: bestandsformaat van de output indien save=True
    '''
    data, fs = read_data(file)
    x = dft(data, L=L, K=K)/(L/512)
    if limits is None:
        maximum = np.ceil(np.log10(np.max(np.absolute(x))))*10
        limits = [maximum-30, maximum]
        print("Spectrum of %s plotted from %ddB to %ddB" % (file, limits[0], limits[1]))
    if plot or save:
        font = {'size':16}
        matplotlib.rc('font', **font)
        x_lim = [0, len(x[channel])*K/fs]
        y_lim = np.fft.rfftfreq(L)[[0, -1]]*fs
        fig = plt.figure(figsize=(8.0, 5.0))
        ax = fig.add_subplot(111)
        im = ax.imshow(np.log10(np.transpose(np.absolute(x[channel]))[::-1])*10, extent=[x_lim[0],x_lim[1],y_lim[0],y_lim[1]], interpolation='gaussian', cmap=cm.get_cmap('inferno'), vmin=limits[0], vmax=limits[1])
        ax.set_aspect('auto')
        plt.xlabel('Tijd [s]')
        plt.ylabel('Frequentie [Hz]')
        cbar = fig.colorbar(im)
        cbar.set_label('Intensiteit [dB]', rotation=270, labelpad=15)
        plt.gcf().subplots_adjust(bottom=0.15, left=0.15, right=0.95)
        if save:
            plt.savefig('%s%s%s' % (os.path.splitext(file)[0], '' if channel == 0 else ("_channel%s" % channel), fileFormat), dpi=300)
        if plot:
            plt.show()

def reconstruct_FFT(data, L=512, K=256):
    '''
    [DEPRECATED]
    Plot de inverse DFT van de DFT van <data>. Dit is een test om te
    controleren of het hanningvenster opnieuw toegepast moet worden bij de
    inverse fouriertransformatie.
    @param data: lijst van lijsten, waarbij deze laatste de samples zijn van
        een bepaalde microfoon
    @param L [512]: lengte van 1 segment waarvan de fouriertransformatie
        genomen wordt
    @param K [256]: afstand waarover verschoven wordt om het volgende segment
        te bekomen voor de berekening van de fouriertransformatie
    '''
    plt.plot(data[0])
    for window in [np.hamming, np.hanning]:
        dft = [np.fft.rfft(data[:,i:i+L]*window(L)) for i in range(0, len(data[0])-L+1, K)]
        data_dft = np.moveaxis(dft, [0, 1], [-2, -3]) # t-MIC-f => MIC-t-f (data in axis=0,1,2)
        nieuwe_data = [[np.fft.irfft(tijdstip) for tijdstip in kanaal] for k, kanaal in enumerate(data_dft)]
        x = [np.arange(i, i+L) for i in range(0, len(data[0])-L+1, L//2)] # Indices overeenkomstig met elke lijst in nieuwe_data
        y = np.zeros((len(data[0])//L)*L) # Nullen even lang als data
        for i, indices in enumerate(x):
            y[indices] += nieuwe_data[0][i]
        plt.plot(y)
    plt.legend(["Data", "Hamming", "Hanning"])
    plt.show()

def MVDR_file(dataFile, noiseFile=None, theta=0, phi=0, L=512, K=256, alpha=1):
    '''
    Past het MVDR-algoritme toe op <dataFile>, met als ruisspectrum het
    spectrum van <noiseFile>, met als beamformrichting (<theta>, <phi>).
    @writes: "output/MVDR/MVDR_<dataFile>_th<theta>_ph<phi>_K.wav"
    @param dataFile: het .wav bestand waarop MVDR toegepast wordt
    @param noiseFile [dataFile]: het .wav bestand waarvan het spectrum
        gebruikt wordt voor de berekening van de covariantiematrices
    @param theta [0]: hoek invalsrichting en neerwaartse verticale [deg]
    @param phi [0]: hoek (in graden) in het horizontale vlak, voor een x-as
        van MIC0 naar midden(1, 6) en de y-as gericht van MIC0 naar MIC2 [deg]
    @param L [512]: lengte van 1 segment waarvan de fouriertransformatie
        genomen wordt
    @param K [256]: afstand waarover verschoven wordt om het volgende segment
        te bekomen voor de berekening van de fouriertransformatie
    @param alpha [1]: verhouding van aanpassing van de covariantiematrix R per
        segment-stap tegenover het behoud van de huidige covariantiematrix
    '''
    print("MVDR van bestand %s met:\n theta = %d\n   phi = %d\n     L = %d\n     K = %d\n alpha = %d" % (dataFile, theta, phi, L, K, alpha))
    if noiseFile == None:
        noiseFile = dataFile
    outdir = 'output/MVDR'
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    data, fs = read_data(dataFile)
    noise = read_data(noiseFile)[0]
    nieuw = MVDR(data, noise=noise, theta=theta, phi=phi, fs=fs, L=L, K=K, alpha=alpha)

    sf.write(os.path.join(outdir, 'MVDR_%s_th%d_ph%d%s%s.wav' % (os.path.splitext(os.path.basename(dataFile))[0], theta, phi, ('_K%d' % K) if K != 256 else '', ('_alpha%d' % (alpha*100)) if alpha != 1 else '')), nieuw.T, fs)


if __name__ == "__main__":
    pass
    # for channel in range(7):
    #     frequencies("data/Matige_snelheid_met_spraak.wav", plot=False, save=True, L=2048, K=128, channel=channel, limits=[-20, 10], fileFormat=".png")
    # ### MVDR BEAMFORMING ###
    # alpha = 0.99
    # MVDR_file("data/Matige_snelheid_met_spraak.wav", theta=0, phi=0, alpha=0.99)
    # MVDR_file("data/8-05/matig_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=alpha)
    # MVDR_file("data/8-05/matig_met_spraak_hoek_45.wav", theta=45, phi=0, alpha=alpha)
    # MVDR_file("data/8-05/matig_met_spraak_hoek_45.wav", theta=0, phi=0, alpha=alpha)
    # MVDR_file("data/8-05/matig_met_spraak_hoek_45-30.wav", theta=45, phi=30, alpha=alpha)
    # MVDR_file("data/8-05/matig_varierend_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=alpha)
    # MVDR_file("data/8-05/vollekracht_met_spraak.wav", theta=0, phi=0, alpha=alpha)
    # MVDR_file("data/8-05/vollekracht_met_spraak_2.wav", theta=0, phi=0, alpha=alpha)
    # MVDR_file("data/8-05/vollekracht_met_spraak_hoek_45.wav", theta=45, phi=0, alpha=alpha)
    # MVDR_file("data/8-05/vollekracht_met_spraak_hoek_45-30.wav", theta=45, phi=30, alpha=alpha)

    # MVDR_file("data/8-05/matig_met_spraak_hoek_45.wav", theta=45, phi=0, alpha=0.90)
    # MVDR_file("data/8-05/matig_met_spraak_hoek_45.wav", theta=45, phi=0, alpha=0.95)
    # MVDR_file("data/8-05/matig_met_spraak_hoek_45.wav", theta=45, phi=0, alpha=0.99)

    # MVDR_file("data/8-05/matig_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=0)
    # MVDR_file("data/8-05/matig_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=0.9)
    # MVDR_file("data/8-05/matig_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=0.95)
    # MVDR_file("data/8-05/matig_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=0.99)
    # MVDR_file("data/8-05/matig_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=1)
    
    # MVDR_file("data/8-05/matig_varierend_met_spraak_hoek_0.wav", theta=0, phi=0, alpha=0)

    # ### MVDR BEAMFORMING PLOTS ###
    # for folder in ["output/MVDR", "data/8-05"]:
    #     for file in os.listdir(folder):
    #         if os.path.splitext(file)[1] == ".wav":
    #             frequencies(os.path.join(folder, file), plot=False, save=True, L=2048, K=128)
    # frequencies(r"output/MVDR\MVDR_matig_met_spraak_hoek_0_th0_ph0_alpha0.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0], fileFormat=".png")
    # frequencies(r"output/MVDR\MVDR_matig_met_spraak_hoek_0_th0_ph0_alpha99.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0], fileFormat=".png")
    # frequencies(r"output/MVDR\MVDR_matig_met_spraak_hoek_0_th0_ph0_alpha95.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0], fileFormat=".png")
    # frequencies(r"output/MVDR\MVDR_matig_met_spraak_hoek_0_th0_ph0_alpha90.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0], fileFormat=".png")
    # frequencies(r"output/MVDR\MVDR_matig_met_spraak_hoek_0_th0_ph0.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0], fileFormat=".png")
    # frequencies(r"output/MVDR\MVDR_matig_met_spraak_hoek_45_th0_ph0_alpha99.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0])
    # frequencies(r"output/MVDR\MVDR_matig_varierend_met_spraak_hoek_0_th0_ph0.wav", plot=False, save=True, L=2048, K=128, limits=[-30, 0])
    # frequencies(r"output/MVDR\MVDR_vollekracht_met_spraak_2_th0_ph0_alpha99.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10], fileFormat=".png")
    # frequencies("data/8-05/matig_met_spraak_hoek_0.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
    # frequencies("data/8-05/matig_met_spraak_hoek_45.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
    # frequencies("data/8-05/matig_met_spraak_hoek_45-30.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
    # frequencies("data/8-05/matig_varierend_met_spraak_hoek_0.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
    # frequencies("data/8-05/vollekracht_met_spraak.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
    # frequencies("data/8-05/vollekracht_met_spraak_2.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10], fileFormat=".png")
    # frequencies("data/8-05/vollekracht_met_spraak_hoek_45.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])
    # frequencies("data/8-05/vollekracht_met_spraak_hoek_45-30.wav", plot=False, save=True, L=2048, K=128, limits=[-20, 10])

    # frequencies("data/MVDR_PostProc_sp64.wav", plot=False, save=True, L=2048, K=128, fileFormat=".png")

    # frequencies(r"output/MVDR\MVDR_Matige_snelheid_met_spraak_th0_ph0_alpha99.wav", plot=False, save=True, L=2048, K=128, limits=[-20,10], fileFormat=".png")

    # ### DS BEAMFORMING ###
    # fileName = 'data/8-05/vollekracht_met_spraak_2.wav'
    # basename = os.path.splitext(os.path.basename(fileName))[0]
    # beamforming(fileName, 0, 0)
    # filter('output/beamforming/beamforming_%s_th0_ph0.wav' % basename)
    # filter(fileName)
    # beamforming('output/n200L60/Processed_%s.wav' % basename, 0, 0)

    # ### DS BEAMFORMING PLOTS ###
    # limits=[-20, 10]
    # save = True
    # plot = False
    # frequencies(fileName, plot=plot, save=save, L=2048, K=128, limits=limits, fileFormat=".png")
    # frequencies("output/beamforming/beamforming_%s_th0_ph0.wav" % basename, plot=plot, save=save, L=2048, K=128, limits=limits, fileFormat=".png")
    # frequencies("output/beamforming/beamforming_Processed_%s_th0_ph0.wav" % basename, plot=plot, save=save, L=2048, K=128, limits=limits, fileFormat=".png")
    # frequencies("output/n200L60/Processed_beamforming_%s_th0_ph0.wav" % basename, plot=plot, save=save, L=2048, K=128, limits=limits, fileFormat=".png")
    # frequencies("output/beamforming/beamforming_45rechts_th45_ph90.wav", plot=plot, save=save, L=2048, K=128, limits=limits)
    # frequencies("output/beamforming/beamforming_45rechts_th45_ph-90.wav", plot=plot, save=save, L=2048, K=128, limits=limits)
    # frequencies("output/beamforming/beamforming_45links_th45_ph90.wav", plot=plot, save=save, L=2048, K=128, limits=limits)
    # frequencies("output/beamforming/beamforming_45links_th45_ph-90.wav", plot=plot, save=save, L=2048, K=128, limits=limits)

    # MVDR_file("data/Matige_snelheid_met_spraak.wav", theta=45, phi=0, alpha=0, noiseFile="data/noise/Matige_snelheid_met_spraak-noise.wav")
    # frequencies("output/MVDR/MVDR_Matige_snelheid_met_spraak_th45_ph0.wav", plot=False, save=True, L=2048, K=128)
    # filter("output/MVDR/MVDR_Matige_snelheid_met_spraak_th45_ph0.wav")
    # frequencies("output/n200L60/Processed_MVDR_Matige_snelheid_met_spraak_th45_ph0.wav", plot=False, save=True, L=2048, K=128)
    # MVDR_file("data/Matige_snelheid_met_spraak.wav", theta=0, phi=0, alpha=0, noiseFile="data/noise/Matige_snelheid_met_spraak-noise.wav")
    # frequencies("output/MVDR/MVDR_Matige_snelheid_met_spraak_th0_ph0.wav", plot=False, save=True, L=2048, K=128)
    # filter("output/MVDR/MVDR_Matige_snelheid_met_spraak_th0_ph0.wav")
    # frequencies("output/n200L60/Processed_MVDR_Matige_snelheid_met_spraak_th0_ph0.wav", plot=False, save=True, L=2048, K=128)