import numpy as np


def R(N):
    '''
    @param N: Aantal microfoons
    @return: Geeft een lijst terug met matrices voor bepaalde frequenties.
        Element 0 komt overeen met frequentie 0 (element 0) in frequentie_n()
    '''
    noise = np.random.normal(0, 0.1, size=(N, 16000)) # 16000 gewoon voor een bepaalde lengte witte ruis, heeft niets met samplefrequentie te maken
    R = covariance_matrix(noise)
    return R


def frequentie_n(fs=16000, L=512):
    '''
    @param fs [16000]: samplefrequentie
    @param L [512]: aantal samples waarover het hanning-venster wordt
        toegepast, en dus het aantal samples dat wordt gebruikt per berekening
        van de fouriertransformatie op een bepaald tijdstip. Het aantal
        frequeties in de DFT is dan floor(L/2) (voor een reëel signaal).
    @return: lijst met frequenties die overeenkomen met de matrix met dezelfde
        index in R
    '''
    return np.fft.rfftfreq(L)*fs


def dft(data, L=512):
    '''
    Geeft een 3D array terug met de fouriertransformatie van <data>. <data> is
    een 2D array, namelijk een lijst van samplewaarden voor elk kanaal.
    De 3D array die wordt teruggegeven bevat de fouriercoëfficiënten voor elke
    frequentie uit np.fft.rfftfreq(<L>), voor het signaal waarop een hamming-
    filter wordt toegepast. Het signaal wordt onderverdeeld in delen met
    lengte <L>, waarbij op elk deel een hammingfilter wordt toegepast.
    @param data: lijst van lijsten, waarbij deze laatste de samples zijn van
        een bepaalde microfoon
    @param L [512]: lengte van een frequentie-bin
    @return: 3D-array, met in de eerste dimensie de verschillende kanalen, de
        tweede dimensie de tijd (onderverdeling per 512 samples), en in de
        derde dimensie de dft voor elke frequentie voor dat deel en kanaal.
    '''
    dft = [np.fft.rfft(data[:,i:i+L]*np.hamming(L)) for i in range(0, len(data[0])-L+1, L//2)]
    return np.moveaxis(dft, [0, 1], [-2, -3]) # t-MIC-f => MIC-t-f (data in axis=0,1,2)


def covariance_matrix(noise):
    '''
    Berekent de covariantiematrix voor elke frequentie in de dft (L=512 dus
    aantal covariantiematrices = 257 (voor alle frequenties van 0 tot 8000))
    @param noise: ruis om de covariantiematrix te berekenen, gegeven als een
        lijst van lijsten, met elke lijst een microfoonkanaal
    @returns: lijst van matrices, elke matrix komt overeen met een frequentie
        uit de DFT. Elke matrix is de covariantiematrix voor die frequentie,
        uitgemiddeld over de tijd.
    '''
    fourierTransform = dft(noise)
    frequencies = [fourierTransform[:,:,i].T for i in range(len(fourierTransform[0][0]))] # 1e dim: freq, 2e dim: tijd, 3e dim: kanalen
    return np.array([np.mean([np.transpose([N])*[N.conj()] for N in freq], axis=0) for freq in frequencies]) # lijst met matrices, elke matrix is R voor een frequentie