import math
import os
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from adfil import read_data, delay, beamform, move, covariance_matrix, weights_MVDR
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D # This import registers the 3D projection, but is otherwise unused.


def gain_beamforming(theta0, phi0, theta, phi, f, theoretical=True, plot=False):
    '''
    Berekent het vermogen van het resulterende signaal na beamforming,
    wanneer een sinusoïdaal signaal als input gebruikt wordt.
    @param theta0: theta van richting van beamforming
    @param phi0: phi van richting van beamforming
    @param theta: richting waaruit het signaal komt
    @param phi: richting waaruit het signaal komt
    @param f: frequentie van sinusoidaal signaal afkomstig uit (theta, phi)
    @param theoretical [True]: indien True wordt een theoretische uitdrukking
        gebruikt om de gain te berekenen, indien False wordt de beamform()-
        functie uit adfil.py gebruikt, met in beide gevallen als input een
        sinusoïdaal signaal met frequentie f afkomstig uit (theta, phi)
    @param plot [False]: True indien men een plot van alle kanalen en het
        beamforming-signaal wenst, False indien niets geplot moet worden
    @return: het resulterende vermogen van de gebeamformde versie van een
        sinusoïdaal signaal (genormaliseerd: gain_beamforming(0,0,0,0,f) = 1)
    '''
    if theoretical:
        d = 0.043 # Afstand van microfoon 0 tot buitenste microfoons
        r = np.array([[0, 0, 0], [math.sqrt(3)/2, .5, 0], [0, 1, 0], [-math.sqrt(3)/2, .5, 0], [-math.sqrt(3)/2, -.5, 0], [0, -1, 0], [math.sqrt(3)/2, -.5, 0]])*d
        exponentials = [np.exp(-1j*2*math.pi*f*(delay(i, 0, theta, phi, r=r) - delay(i, 0, theta0, phi0, r=r))) for i in range(0, len(r))]
        gain = np.absolute(np.sum(exponentials)/7)**2
    else:
        delays = [delay(0, i, theta, phi) for i in range(0, 7)] # 'Experimenteel': enkel delays gebruiken voor generatie van sinussen
        dt = 0.1
        t = np.arange(0, dt, 1/16000)
        delta_t = t[1] - t[0]
        sinuses = [np.sin(f*2*math.pi*(t-delays[i])) for i in range(0, 7)]
        s = beamform(sinuses, delta_t, theta0, phi0)
        if plot:
            plt.plot(t, np.transpose(sinuses))
            plt.plot(t, s, "--")
            plt.show()
        gain = 2*np.sum(s**2)*delta_t/dt # *delta_t wegens integratie, *2 wegens int(sin(2*pi*t), t=0..1)=0.5
    return gain

def plot(theta0, phi0, theoretical=True, plot3D=False, detail=1):
    '''
    Plot de gain in functie van de theta-component van de invalshoek, en de
    frequentie van het invallende sinusoïdale signaal, bij beamforming in de
    (theta0, phi0)-richting. (Voor de phi-component van het invallende signaal
    wordt dezelfde parameter <phi0> gebruikt)
    @writes: "plots/beamforming/plot<'3D' if plot3D else '2D'>-th_<theta0>-ph_<phi0>_<theoretical>.pdf"
    @param theta0: theta van richting van beamforming
    @param phi0: phi van richting van beamforming
    @param theoretical [True]: indien True wordt een theoretische uitdrukking
        gebruikt om de gain te berekenen, indien False wordt de beamform()-
        functie uit adfil.py gebruikt
    @param plot3D [False]: indien True wordt een 3D oppervlak geplot, indien
        False is dit een 2D oppervlak
    @param detail [1]: factor die weergeeft hoe gedetailleerd de figuur moet
        zijn, waarbij hogere waarden voor een gedetailleerdere figuur zorgen
    '''
    theta_range = np.linspace(-90, 90, 50*detail)
    f_range = np.linspace(20, 7990, 80*detail)
    theta_gain = np.array([[gain_beamforming(theta0, phi0, theta, phi0, f, theoretical=theoretical) for theta in theta_range] for f in f_range])
    theta_gain = np.maximum(10*np.log10(theta_gain), np.zeros_like(theta_gain)-30)

    outdir = 'plots/beamforming'
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
        
    fig = plt.figure()
    if plot3D:
        ax = fig.gca(projection='3d')
        theta_range, f_range = np.meshgrid(theta_range, f_range)
        ax.plot_surface(f_range, theta_range, theta_gain, cmap=cm.get_cmap('jet'), linewidth=0, antialiased=False)
    else:
        font = {'size':16}
        matplotlib.rc('font', **font)
        ax = fig.add_subplot(111)
        im = ax.imshow(theta_gain, extent=[min(theta_range), max(theta_range), max(f_range), min(f_range)], cmap=cm.get_cmap('jet'))
        ax.set_aspect('auto')
        cbar = fig.colorbar(im)
        cbar.set_label('Directiviteit [dB]', rotation=270, labelpad=15)
        plt.xlabel('$\\theta$ [°]')
        plt.ylabel('$f$ [Hz]')
        plt.gcf().subplots_adjust(bottom=0.15, left=0.2)
    plt.savefig(os.path.join(outdir, 'plot%s-th_%d-ph_%d_%s.png' % ('3D' if plot3D else '2D', theta0, phi0, theoretical)))
    plt.show()

def plot_MVDR(theta0, phi0, plot3D=False, detail=1, noiseFile=None):
    '''
    Plot de gain in functie van de theta-component van de invalshoek, en de
    frequentie van het invallende sinusoïdale signaal, bij beamforming in de
    (theta0, phi0)-richting. (Voor de phi-component van het invallende signaal
    wordt dezelfde parameter <phi0> gebruikt)
    @writes: "plots/MVDR/plot<'3D' if plot3D else '2D'>-th_<theta0>-ph_<phi0>_<noiseFile>.pdf"
    @param theta0: theta van richting van beamforming
    @param phi0: phi van richting van beamforming
    @param plot3D [False]: indien True wordt een 3D oppervlak geplot, indien
        False is dit een 2D oppervlak
    @param detail [1]: factor die weergeeft hoe gedetailleerd de figuur moet
        zijn, waarbij hogere waarden voor een gedetailleerdere figuur zorgen
    @param noiseFile [None]: bestand met ruis, waarvan de covariantiematrix
        berekend wordt, indien niet gespecifieerd wordt witte ruis gebruikt
    '''
    if noiseFile is None:
        fs = 16000
        noise = np.random.normal(0, 0.1, size=(7, 4*fs))
    else:
        noise, fs = read_data(noiseFile)
    
    theta_range = np.linspace(-90, 90, 50*detail)
    f_range = np.fft.rfftfreq(512)*fs

    R = covariance_matrix(noise)
    w = weights_MVDR(R, theta0, phi0, fs=fs)
    theta_gain = np.array([[np.absolute(np.dot(w_freq.conj().T, np.array([[np.exp(-1j*2*math.pi*f_range[f]*delay(i, 0, theta, phi0)) for i in range(0, 7)]]).T)[0,0]) for theta in theta_range] for f, w_freq in enumerate(w)])
    theta_gain = np.maximum(10*np.log10(theta_gain), np.zeros_like(theta_gain)-30)

    outdir = 'plots/MVDR'
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
        
    fig = plt.figure()
    if plot3D:
        ax = fig.gca(projection='3d')
        theta_range, f_range = np.meshgrid(theta_range, f_range)
        ax.plot_surface(f_range, theta_range, theta_gain, cmap=cm.get_cmap('jet'), linewidth=0, antialiased=False)
    else:
        font = {'size':16}
        matplotlib.rc('font', **font)
        ax = fig.add_subplot(111)
        im = ax.imshow(theta_gain, extent=[min(theta_range), max(theta_range), max(f_range), min(f_range)], cmap=cm.get_cmap('jet'))
        ax.set_aspect('auto')
        cbar = fig.colorbar(im)
        cbar.set_label('Directiviteit [dB]', rotation=270, labelpad=15)
        plt.xlabel('$\\theta$ [°]')
        plt.ylabel('$f$ [Hz]')
        plt.gcf().subplots_adjust(bottom=0.15, left=0.2)
    plt.savefig(os.path.join(outdir, 'plot%s-th_%d-ph_%d_%s.pdf' % ('3D' if plot3D else '2D', theta0, phi0, os.path.splitext(os.path.basename('%s' % noiseFile))[0])))
    plt.show()

def plot_beam(f, theta0=0, phi0=0, theoretical=True):
    '''
    Toont een 3D sferische plot van de gain voor een invallend sinusoïdaal
    signaal met frequentie <f> uit elke richting, voor een bepaalde gewenste
    beamforming-richting (theta0, phi0) en invallende frequentie f.
    @param f: frequentie van invallend sinusoïdaal signaal
    @param theta0: theta-hoek van richting van beamforming
    @param phi0: phi-hoek van richting van beamforming
    @param theoretical [True]: indien True wordt een theoretische uitdrukking
        gebruikt om de gain te berekenen, indien False wordt de beamform()-
        functie uit adfil.py gebruikt, met in beide gevallen als input een
        sinusoïdaal signaal met frequentie f
    '''
    theta_range = np.linspace(0, 180, 25)
    phi_range = np.linspace(-180, 180, 50)
    gains = np.array([[gain_beamforming(theta0, phi0, theta, phi, f) for theta in theta_range] for phi in phi_range])

    THETA, PHI = np.meshgrid(theta_range/180*math.pi, phi_range/180*math.pi)
    min_dB = 30
    R = np.maximum(10*np.log10(gains)+min_dB, np.zeros_like(gains))
    X, Y, Z = R*np.sin(THETA)*np.cos(PHI), R*np.sin(THETA)*np.sin(PHI), R*np.cos(THETA)
    fig = plt.figure()
    fig.suptitle('%d$Hz$ ($\\theta_0=%d$°, $\\phi_0=%d$°)' % (f, theta0, phi0), fontsize=16)
    ax = fig.add_subplot(1,1,1, projection='3d')
    ax.set_aspect(1)
    cmap = cm.get_cmap('jet')
    ax.plot_surface(X, Y, Z, rstride=1, cstride=1, linewidth=0, antialiased=False, alpha=0.5,
                    facecolors=cmap(np.append(R[:,:len(R[0])//2], R[:,len(R[0])//2+1:], axis=1)/min_dB)) # Rare cmap zodat symmetrisch

    # Create cubic bounding box to simulate equal aspect ratio
    max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max()
    Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(X.max()+X.min())
    Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(Y.max()+Y.min())
    Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(Z.max()+Z.min())
    for xb, yb, zb in zip(Xb, Yb, Zb):
        ax.plot([xb], [yb], [zb], 'w')
    # End of setting equal aspect ratio
    
    try:
        plt.get_current_fig_manager().window.showMaximized()
    except:
        pass
    plt.show()


if __name__ == "__main__":
    theoretical, theta0 = True, 0
    detail = 4
    noiseFile = "data/8-05/vollekracht_met_spraak_2.wav"
    # noiseFile = None
    # plot_beam(1000, theta0=theta0, theoretical=theoretical)
    # plot_beam(2000, theta0=theta0, theoretical=theoretical)
    # plot_beam(4000, theta0=theta0, theoretical=theoretical)
    # plot_beam(6000, theta0=theta0, theoretical=theoretical)
    # plot_beam(6000, theta0=theta0, theoretical=not theoretical)
    # plot_MVDR(0, 0, noiseFile=noiseFile, detail=detail)
    # plot_MVDR(45, 0, noiseFile=noiseFile, detail=detail)
    # plot_MVDR(0, 30, noiseFile=noiseFile, detail=detail)
    # plot_MVDR(45, 30, noiseFile=noiseFile, detail=detail)
    plot(0, 30, detail=4)
    plot(45, 30, detail=4)