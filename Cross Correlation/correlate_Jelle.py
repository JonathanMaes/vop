# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 13:58:47 2019

@author: Jelle Michiels
"""

import math
import numpy as np
import matplotlib.pyplot as plt
import soundfile as sf
import os

def read_data(path, isDirectory=False):
    '''
        Leest een .wav bestand
        path: locatie van een .wav bestand of locatie van een map met .wav bestanden
        Returns: list of lists, with each list a microphone channel
        
    '''
    if os.path.isfile(path):
        data, fs = sf.read(path)
        data = data.transpose()
        data = data
        times = np.arange(len(data))/fs
        return [data, times]

def get_all(path=0, filename=0):
    if path == 0:
        path = 'D:/Jelle Michiels/Documenten/Audacity/'
    if filename == 0:
        filename = 'arrayaandrone+klap-'
    mic0 = read_data(path + filename + '01.wav')[0]
    mic1 = read_data(path + filename + '02.wav')[0]
    mic2 = read_data(path + filename + '03.wav')[0]
    mic3 = read_data(path + filename + '04.wav')[0]
    mic4 = read_data(path + filename + '05.wav')[0]
    mic5 = read_data(path + filename + '06.wav')[0]
    mic6 = read_data(path + filename + '07.wav')[0]
    t = read_data(path + filename + '07.wav')[1]
    return [mic0, mic1, mic2, mic3, mic4, mic5, mic6, t]

def adfil(M,L,mu,micnum, path=0, filename=0):
    if path == 0:
        path = 'D:/Jelle Michiels/Documenten/Audacity/'
    if filename == 0:
        filename = 'samen-'
    x1 = read_data(path+filename+ '0' + str(micnum+1) + '.wav')
#    x2 = read_data(path+filename+ '0' + str(micnum+2) + '.wav')
    x = x1[0]
    # t = x1[1]
    l = len(x)
#    xx = x2[0]
    
    
#    r = 0/10*np.random.randn(l)
#    x = x1 + r
    
    y = np.zeros(l)
#    yy = np.zeros(l)
    w = np.zeros(M)
    e = np.zeros(l)
#    ee = np.zeros(l)
    for k in range(M+L,l):
        y[k] = np.dot(w, x[k-M-L:k-L])
#        yy[k] = np.dot(w, xx[k-M-L:k-L])
        e[k] = x[k] - y[k]
#        ee[k] = xx[k] - yy[k]
        w = np.add(w, 2*mu*e[k]*x[k-M-L:k-L]/(np.dot(x[k-M-L:k-L],x[k-M-L:k-L])+0.01))
    return e
        
#    plt.figure()
#    plt.plot(t, xx)
#    plt.title('Original Signal')
#    plt.show()
#    
#    plt.figure()
#    plt.plot(t, yy)
#    plt.title('Predicted Signal')
#    plt.show()
#    
#    plt.figure()
#    plt.plot(t, yy-xx)
#    plt.title('Error')
#    plt.show()
#    
    # sf.write(path + filename + '0' + str(micnum+1) + 'processed.wav', e, 16000)
#    sf.write(path + filename + '0' + str(micnum+2) + 'processed.wav', ee, 16000)

def correlate(a, b, k):
    l = np.zeros(2*k+1)
    i = 0
    for n in range(k,0,-1):
        l[i] = np.dot(b[n:],a[0:len(a)-n])
        i += 1
    for n in range(0, k+1):
        l[n+k] = np.dot(a[n:],b[0:len(b)-n])
        
    plt.figure()
    plt.plot(l)
    plt.title('Error')
    plt.show()
        
    return np.argmax(l)-k
        
        
    
def do_all(M,L,mu):
    mic0 = adfil(M,L,mu,0)
    mic1 = adfil(M,L,mu,1)
    mic2 = adfil(M,L,mu,2)
    mic3 = adfil(M,L,mu,3)
    mic4 = adfil(M,L,mu,4)
    mic5 = adfil(M,L,mu,5)
    mic6 = adfil(M,L,mu,6)
    a = [correlate(mic0, mic1,50), correlate(mic0,mic2,50), correlate(mic0,mic3,50), correlate(mic0,mic4,50), correlate(mic0,mic5,50), correlate(mic0,mic6,50)]
    ma = max(a)
    mi = min(a)
    if mi < 0:
        nmic0 = np.concatenate(np.zeros(-mi), mic0)
    if ma > 0:
        nmic0 = np.concatenate(nmic0, np.zeros(ma))
    if a[0] < 0 and mi < 0:
        nmic1 = np.concatenate(np.zeros(abs(mi)+a[0]),mic1)
        nmic1 = np.concatenate(nmic1, np.zeros(len(nmic0)-len(nmic1)))
    else:
        nmic1 = np.concatenate(np.zeros(abs(abs(mi)-a[0])),mic1)
        nmic1 = np.concatenate(nmic1, np.zeros(len(nmic0)-len(nmic1)))

    
    
    e=mic0[18:]+mic1[:-18]
    sf.write('D:/Users/Jelle Michiels/Documenten/Audacity/try.wav',e,16000)
            
mic0 = adfil(60,100,0.1,0)
mic1 = adfil(60,100,0.1,1)
mic2 = adfil(60,100,0.1,2)
#mic3 = adfil(60,100,0.1,3)
#mic4 = adfil(60,100,0.1,4)
#mic5 = adfil(60,100,0.1,5)
#mic6 = adfil(60,100,0.1,6)
a = correlate(mic0,mic1,50)
b = correlate(mic0,mic2,50)
#c = correlate(mic0,mic3,16000)
#d = correlate(mic0,mic4,16000)
#e = correlate(mic0,mic5,16000)
#f = correlate(mic0,mic6,16000)
#g = correlate(mic1,mic2,16000)
#h = correlate(mic1,mic3,16000)
#i = correlate(mic1,mic4,16000)
#j = correlate(mic1,mic5,16000)
#k = correlate(mic1,mic6,16000)