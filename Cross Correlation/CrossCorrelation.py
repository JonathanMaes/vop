# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 16:38:34 2019

@author: Hanne Herssens
"""

import math
import numpy as np
import matplotlib.pyplot as plt
import soundfile as sf
import os

def read_data(path, isDirectory=False):
    '''
        Leest een .wav bestand
        path: locatie van een .wav bestand of locatie van een map met .wav bestanden
        Returns: list of lists, with each list a microphone channel
        
    '''
    if os.path.isfile(path):
        data, fs = sf.read(path)
        data = data.transpose()
        data = [data]
        times = np.arange(len(data[0]))/fs
        return [data, times]
    
x = read_data('output/Processed_audiotrack-5-16000.wav')[0][0]
y = read_data('output/Processed_audiotrack-6-16000.wav')[0][0]

N = len(x)
b = 52.222 # duur meting
time = np.arange(1-N,N)
cross_correlation = np.correlate(x,y,'full')
delay = time[cross_correlation.argmax()] *1.0* b/N

textstr = 'Delay = ' + "{:.2e}".format(delay) + 's'
fig, ax = plt.subplots()
plt.plot(cross_correlation)
plt.title('Cross Correlation of Microphone 5 and 6')
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=14,
        verticalalignment='top', bbox=props)
#plt.savefig('CC56')
