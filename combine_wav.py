import os
import sys
import soundfile as sf
import numpy as np


def combine(folder, n=8):
    '''
    Leest alle .wav bestanden in de map <folder> in standaard volgorde in, en
    voegt deze samen tot 1 bestand, waarbij de kanalen van dit bestand gevormd
    worden door de oorspronkelijke bestanden in <folder>.
    @writes: "<folder>.wav"
    @param folder: de map waarvan alle .wav bestanden worden samengevoegd als
        individuele kanalen in een nieuw bestand
    '''
    data = []
    for f in os.listdir(folder)[:n]:
        if f.endswith(".wav"):
            d, fs = sf.read(os.path.join(folder, f))
            data.append(d)
    if len(data) < n:
        zeroes = np.zeros((n, len(data[0])))
        zeroes[:len(data)] = data
        data = zeroes
    output = os.path.join(os.path.dirname(folder), '%s.wav' % os.path.basename(folder))
    sf.write(output, np.transpose(np.array(data)), fs)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        folder = sys.argv[1]
    else:
        folder = input('Folder: ')
    combine(folder)